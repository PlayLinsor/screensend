﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScreenSend
{
    public class Settings
    {
        bool Tranzaction = false;


        public Settings()
        {
            var Options = Properties.Settings.Default;
            if (Options.NeedUpgradeSettings)
            {
                Options.Upgrade();
                Options.NeedUpgradeSettings = false;
                Options.Save();
            }
        }

        public void StartTranzaction()
        {
            Tranzaction = true;
        }

        public void EndTranzaction()
        {
            Tranzaction = false;
            Save();
        }

        public void Save()
        {
            if (!Tranzaction) Properties.Settings.Default.Save();
        }

        public string SMTP_Server
        {
            set
            {
                Properties.Settings.Default.SMTPServer =  value;
                Save();
            }
            get { return Properties.Settings.Default.SMTPServer; }
        }

        public string SMTP_Port
        {
            set
            {
                Properties.Settings.Default.SMTPPort = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTPPort; }
        }

        public string SMTP_User
        {
            set
            {
                Properties.Settings.Default.SMTPUser = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTPUser; }
        }

        public string SMTP_Password
        {
            set
            {
                Properties.Settings.Default.SMTPPassword = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTPPassword; }
        }

        public string SMTP_REZ_Server
        {
            set
            {
                Properties.Settings.Default.SMTP2Server = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTP2Server; }
        }

        public string SMTP_REZ_Port
        {
            set
            {
                Properties.Settings.Default.SMTP2Port = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTP2Port; }
        }

        public string SMTP_REZ_User
        {
            set
            {
                Properties.Settings.Default.SMTP2User = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTP2User; }
        }

        public string SMTP_REZ_Password
        {
            set
            {
                Properties.Settings.Default.SMTP2Password = value;
                Save();
            }
            get { return Properties.Settings.Default.SMTP2Password; }
        }


        public string Mail_From
        {
            set
            {
                Properties.Settings.Default.MailFrom = value;
                Save();
            }
            get { return Properties.Settings.Default.MailFrom; }
        }

        public string HooKKey
        {
            set
            {
                Properties.Settings.Default.HookKey = value;
                Save();
            }
            get { return Properties.Settings.Default.HookKey; }
        }

        public string Mail_Subject
        {
            set
            {
                Properties.Settings.Default.Subject = value;
                Save();
            }
            get { return Properties.Settings.Default.Subject; }
        }
    
    }
}
