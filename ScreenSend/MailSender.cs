﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Mail;

namespace ScreenSend
{
    class MailSender
    {
        System.ComponentModel.BackgroundWorker MailThead = new System.ComponentModel.BackgroundWorker();
        public delegate void EventMailSend(string status);
        public event EventMailSend OnSendMail = null; 

        public MailSender()
        {
            MailThead.DoWork += new System.ComponentModel.DoWorkEventHandler(MailSender_DoWork);
            MailThead.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(MailSender_RunWorkerCompleted); 
        }

        void MailSender_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string[] param = e.Argument as string[];
            string AttachmentFilePath = param[1];
            int CurrentParmSMTP = Convert.ToInt32(param[0]);

            int port;
            string smtpAddress, emailFrom, password;
            bool enableSSL;

            

            Settings cSettings = new Settings();

            if (CurrentParmSMTP == 1)
            {
                port = Convert.ToInt32(cSettings.SMTP_Port);
                enableSSL = true;

                smtpAddress = cSettings.SMTP_Server; // адрес stmp сервера
                emailFrom = cSettings.SMTP_User; // адрес почты отправителя письма
                password = cSettings.SMTP_Password; // пароль почты отправителя письма
            }
            else
            {
                port = Convert.ToInt32(cSettings.SMTP_REZ_Port);
                enableSSL = true;

                smtpAddress = cSettings.SMTP_REZ_Server; // адрес stmp сервера
                emailFrom = cSettings.SMTP_REZ_User; // адрес почты отправителя письма
                password = cSettings.SMTP_REZ_Password; // пароль почты отправителя письма
            }

            string emailTo = cSettings.Mail_From; // адрес почты получателя письма
            string subject = cSettings.Mail_Subject; // тема письма
            string body = "<strong>Письмо отправлено с помощью программы ScreenSend http://salactor.ru </strong><br><br>"; // текст письма
            if (CurrentParmSMTP == 1) body += "\n\nОтправлено с основного SMTP";
            else body += "\n\nВНИМАНИЕ! Отправлено с резервного SMTP";


            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(emailFrom);
            mail.To.Add(emailTo);
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            if (AttachmentFilePath != "")
                mail.Attachments.Add(new Attachment(AttachmentFilePath)); // если нужно прикрепить текстовый файл

            using (SmtpClient smtp = new SmtpClient(smtpAddress, port))
            {
                smtp.Credentials = new NetworkCredential(emailFrom, password);
                smtp.EnableSsl = enableSSL;
                try
                {
                    smtp.Send(mail);
                    e.Result = "OK";
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        e.Result = ex.InnerException.Message;
                    } else e.Result = ex.Message.ToString();
                }
            }
        }

        public void MailSend(string AttachmentFilePath, int CurrentSMTP)
        {
            if (!MailThead.IsBusy) MailThead.RunWorkerAsync(new string[] { CurrentSMTP.ToString(), AttachmentFilePath });
        }

        void MailSender_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            OnSendMail(e.Result.ToString());
        }

        
    }
}
