﻿using System;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.IO;


namespace ScreenSend
{
    class ScreenImage
    {
        string _TemporyDir;
        string TemporyPath = null;

        public string CreateScreenShoot()
        {
            Graphics graph = null;
            var bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            graph = Graphics.FromImage(bmp);
            graph.CopyFromScreen(0, 0, 0, 0, bmp.Size);

            TemporyPath = CreateOrQueryTempDir() + CreateDataFileName();
            

            try
            {
                bmp.Save(TemporyPath);
            }
            catch { return TemporyPath; }
            return TemporyPath;
        }

        private string CreateDataFileName()
        {
            DateTime DT = new DateTime();
            DT = DateTime.Now;
            string RetText = string.Format("{0}-{1}-{2}[{3}_{4}_{5}]).jpg", DT.Month, DT.Day, DT.Year, DT.Hour, DT.Minute, DT.Second);

            return RetText;
           
        }


        private string CreateOrQueryTempDir()
        {
            string Tempory = "";

            if (_TemporyDir == null)
            {
                Tempory = System.IO.Path.GetTempPath() + "ScreenSendRes\\";

                if (!System.IO.Directory.Exists(Tempory))
                {
                    System.IO.Directory.CreateDirectory(Tempory);
                }
            }
            else Tempory = _TemporyDir;

            return Tempory;
        }

        public string GetImagePath()
        {
            return TemporyPath;
        }

    }
}
