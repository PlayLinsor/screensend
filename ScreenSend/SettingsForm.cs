﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace ScreenSend
{
    public partial class SettingsForm : Form
    {
        #region Начальная иницилизация
        HookKey Hk;                             // Хуки
        ScreenImage SI = new ScreenImage();     // Скриншоты
        Settings curSetting = new Settings();     // Текущие настройки
        MailSender Mail = new MailSender();     // Отправка емайла
        Timer StatusIconVisible;
        bool FlagLastFaled = false;
        string LastFailed;

        int VisibleIconSec = 0;

        #endregion

        public SettingsForm()
        {
            InitializeComponent();
            StatusIconVisible = new Timer();
            StatusIconVisible.Interval = 1000;
            StatusIconVisible.Tick += new EventHandler(StatusIconVisible_Tick);
            Mail.OnSendMail += new MailSender.EventMailSend(Mail_OnSendMail);


            Hk = new HookKey(this.Handle,GetHookControlKey(),GetHookKey()); // Куда посылать события
            // Загрузка настроек

            textBox1.Text = curSetting.Mail_From;
            textBox6.Text = curSetting.Mail_Subject;
            textBox2.Text = BuatifulHooker(curSetting.HooKKey);

            textBox7.Text = curSetting.SMTP_Server;
            textBox3.Text = curSetting.SMTP_User;
            textBox4.Text = curSetting.SMTP_Password ;
            textBox5.Text = curSetting.SMTP_Port;

            textBox8.Text = curSetting.SMTP_REZ_Server;
            textBox9.Text = curSetting.SMTP_REZ_User;
            textBox11.Text = curSetting.SMTP_REZ_Password;
            textBox10.Text = curSetting.SMTP_REZ_Port;

        }

        private uint GetHookControlKey()
        {
            uint all = 0;
            string[] keys = curSetting.HooKKey.Split('+');
            if (keys.Length == 1)
            {
                return 0;
            }
            else
            {
                for (int i = 0; i < keys.Length-1; i++)
                {
                    switch (keys[i])
                    {
                        case "ALT": all += 1; break;
                        case "CTRL": all += 2; break;
                        case "SHIFT": all += 4 ; break;
                    } 
                }
            }

            return all;
        }

        private uint GetHookKey()
        {
            string[] keys = curSetting.HooKKey.Split('+');

            return Convert.ToUInt32(keys[keys.Length - 1]); 
        }

        // Обработка клавиш
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312)
            {
                if (m.WParam.ToInt32() == 0)
                {
                    ShootAndSend(1);
                }

            }
            base.WndProc(ref m);
        }

        private void ShootAndSend(int SMTP)
        {
            ToolIcon.Icon = Properties.Resources.Send;
            SI.CreateScreenShoot();

           
            Mail.MailSend(SI.GetImagePath(),SMTP);
        }

        void StatusIconVisible_Tick(object sender, EventArgs e)
        {
            if (VisibleIconSec++ == 10)
            {
                if (FlagLastFaled) ToolIcon.Icon = Properties.Resources.Send;
                else ToolIcon.Icon = Properties.Resources.telegram;
                VisibleIconSec = 0;
                StatusIconVisible.Stop();
            }
        }

        private string BuatifulHooker(string SettingsHook)
        {
            string[] keys = curSetting.HooKKey.Split('+');
            string text = "";

            if (keys.Length > 1)
            {
                for (int i = 0; i < keys.Length - 1; i++)
                {
                    text += keys[i] + "+"; 
                }
            }

            text += (Keys)Convert.ToUInt32(keys[keys.Length - 1]);
            return text; 
        }

        // Редактор хуков
        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (!textBox2.ReadOnly)
            {
                
                string SumKeys = "";
                if (e.Alt) SumKeys += "ALT+";
                if (e.Control) SumKeys += "CTRL+";
                if (e.Shift) SumKeys += "SHIFT+";
                if (!(e.KeyValue >= 16 && e.KeyValue <= 18))
                {
                    textBox2.Tag = SumKeys + e.KeyValue;
                    SumKeys += (Keys)e.KeyValue ;
                }
                e.SuppressKeyPress = false;

                textBox2.Text = SumKeys.ToString();
               
            }
        }

        // Сообщение об отправки сообщения
        void Mail_OnSendMail(string status)
        {
            if (status == "OK")
            {
                ToolIcon.Icon = Properties.Resources.Susses;
                StatusIconVisible.Start();
                ToolIcon.BalloonTipText = "Успешно отправлено";
                ToolIcon.ShowBalloonTip(70000);
            }
            else
            {
                ToolIcon.Icon = Properties.Resources.Failed;
                StatusIconVisible.Start();
                ToolIcon.BalloonTipText = status;
                ToolIcon.ShowBalloonTip(70000);
                if (!FlagLastFaled)
                {
                    LastFailed = status;
                    ShootAndSend(2);
                    FlagLastFaled = true;
                }
                else
                {
                    MessageBox.Show("Внимание, ошибка обоих SMTP серверов!\nSMTP 1:" + LastFailed + "\nSMTP 2:" + status, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #region Настройки Интерфейс

        private void SettingsForm_Shown(object sender, EventArgs e)
        {
            Visible = false;
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolIcon.Icon = null;
            Application.Exit();
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            curSetting.StartTranzaction();

            curSetting.Mail_From = textBox1.Text;
            curSetting.Mail_Subject = textBox6.Text;

            curSetting.SMTP_Server = textBox7.Text;
            curSetting.SMTP_User = textBox3.Text;
            curSetting.SMTP_Password = textBox4.Text;
            curSetting.SMTP_Port = textBox5.Text;
            curSetting.SMTP_REZ_Server = textBox8.Text;
            curSetting.SMTP_REZ_User = textBox9.Text;
            curSetting.SMTP_REZ_Password = textBox11.Text;
            curSetting.SMTP_REZ_Port = textBox10.Text;
            

            curSetting.EndTranzaction();

            Visible = false;
            if (e.CloseReason.ToString() == "UserClosing") e.Cancel = true;
        }
        
        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Focus();
            tabControl1.SelectedIndex = 0;
            Visible = true;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 3;
            Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ToolIcon.Icon = Properties.Resources.Send;
        }

        private void сделатьИОтправитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShootAndSend(1);
        }
#endregion

        private void button3_Click(object sender, EventArgs e)
        {
            if (!textBox2.ReadOnly)
            {
                curSetting.HooKKey = textBox2.Tag.ToString();
                button4.Enabled = false;
                textBox2.ReadOnly = true;
                Hk.SetHooK(GetHookControlKey(), GetHookKey());
            }
            else
            {
                button4.Enabled = true;
                textBox2.ReadOnly = false;
            }
        }

        


        

       

        
       
    }
}
